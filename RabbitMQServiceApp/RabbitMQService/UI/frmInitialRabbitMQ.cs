﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RabbitMQService.UI
{
    public partial class frmInitialRabbitMQ : Form
    {
        IConnection _rabbitMQConnection;
        IModel _emailChanel;
        IModel _smsChanel;

        public frmInitialRabbitMQ()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var connection = new ConnectionFactory();
                connection.HostName = "localhost";
                connection.Port = 5672;
                connection.UserName = "demo";
                connection.Password = "demo";
                connection.VirtualHost = "ClientAppVH";
                connection.AutomaticRecoveryEnabled = true;
                connection.DispatchConsumersAsync = true;

                _rabbitMQConnection = connection.CreateConnection("RabbitMQConnection");
                MessageBox.Show("RabbitMQ connection established.", "Connected", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                using (var connection = _rabbitMQConnection.CreateModel())
                {
                    connection.ExchangeDeclare("Notification", ExchangeType.Direct, true, false);
                    MessageBox.Show("Exchange created successfully.", "Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                using (var connection = _rabbitMQConnection.CreateModel())
                {
                    connection.QueueDeclare("Email", true, false, false);
                    connection.QueueDeclare("SMS", true, false, false);
                    MessageBox.Show("Queues created successfully.", "Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                using (var connection = _rabbitMQConnection.CreateModel())
                {
                    connection.QueueBind("Email", "Notification", "email");
                    connection.QueueBind("SMS", "Notification", "sms");
                    MessageBox.Show("Queues are bind with exchange successfully.", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                using (var chanel = _rabbitMQConnection.CreateModel())
                {
                    var emailText = emailTextBox.Text;

                    var properties = chanel.CreateBasicProperties();
                    properties.DeliveryMode = 2;
                    chanel.BasicPublish("Notification", "email", properties, UTF8Encoding.UTF8.GetBytes(emailText));
                    MessageBox.Show("Message published successfully.", "Published", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                using (var chanel = _rabbitMQConnection.CreateModel())
                {
                    var smsText = smsTextBox.Text;

                    var properties = chanel.CreateBasicProperties();
                    properties.DeliveryMode = 2;
                    chanel.BasicPublish("Notification", "sms", properties, UTF8Encoding.UTF8.GetBytes(smsText));
                    MessageBox.Show("Message published successfully.", "Published", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEmailSubscribe_Click(object sender, EventArgs e)
        {
            try
            {
                _emailChanel = _rabbitMQConnection.CreateModel();
                _emailChanel.BasicQos(0, 1, false);
                var emailConsumer = new AsyncEventingBasicConsumer(_emailChanel);
                emailConsumer.Received += EmailConsumer_Received;
                _emailChanel.BasicConsume("Email", false, emailConsumer);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async Task EmailConsumer_Received(object sender, BasicDeliverEventArgs e)
        {
            var emailMessage = Encoding.UTF8.GetString(e.Body.ToArray());
            emailContentListBox.Invoke((MethodInvoker)(() => emailContentListBox.Items.Add(emailMessage)));
            _emailChanel.BasicAck(e.DeliveryTag, false);
        }

        private void btnSMSSubscribe_Click(object sender, EventArgs e)
        {
            try
            {
                _smsChanel = _rabbitMQConnection.CreateModel();
                _smsChanel.BasicQos(0, 1, false);
                var smsConsumer = new AsyncEventingBasicConsumer(_smsChanel);
                smsConsumer.Received += SmsConsumer_Received;
                _smsChanel.BasicConsume("SMS", false, smsConsumer);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async Task SmsConsumer_Received(object sender, BasicDeliverEventArgs e)
        {
            var smsContent = Encoding.UTF8.GetString(e.Body.ToArray());
            smsContentListBox.Invoke((MethodInvoker)(()=> smsContentListBox.Items.Add(smsContent)));
            _smsChanel.BasicAck(e.DeliveryTag, false);
        }
    }
}

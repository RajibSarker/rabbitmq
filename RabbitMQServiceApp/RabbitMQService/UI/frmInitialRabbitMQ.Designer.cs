﻿namespace RabbitMQService.UI
{
    partial class frmInitialRabbitMQ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.smsTextBox = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.emailContentListBox = new System.Windows.Forms.ListBox();
            this.btnEmailSubscribe = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.smsContentListBox = new System.Windows.Forms.ListBox();
            this.btnSMSSubscribe = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(180, 50);
            this.button1.TabIndex = 0;
            this.button1.Text = "Connect To RabbitMQ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(198, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(188, 50);
            this.button2.TabIndex = 1;
            this.button2.Text = "Create New Exchange";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(392, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(197, 50);
            this.button3.TabIndex = 2;
            this.button3.Text = "Create Queues";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(595, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(193, 50);
            this.button4.TabIndex = 3;
            this.button4.Text = "Bind Queues With Exchange";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(95, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Publish Email";
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(198, 88);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(271, 27);
            this.emailTextBox.TabIndex = 5;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(475, 87);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(94, 29);
            this.button5.TabIndex = 6;
            this.button5.Text = "Publish";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(107, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "PublishSMS";
            // 
            // smsTextBox
            // 
            this.smsTextBox.Location = new System.Drawing.Point(198, 135);
            this.smsTextBox.Name = "smsTextBox";
            this.smsTextBox.Size = new System.Drawing.Size(271, 27);
            this.smsTextBox.TabIndex = 5;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(475, 134);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(94, 29);
            this.button7.TabIndex = 6;
            this.button7.Text = "Publish";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 214);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Email Content";
            // 
            // emailContentListBox
            // 
            this.emailContentListBox.FormattingEnabled = true;
            this.emailContentListBox.ItemHeight = 20;
            this.emailContentListBox.Location = new System.Drawing.Point(12, 237);
            this.emailContentListBox.Name = "emailContentListBox";
            this.emailContentListBox.Size = new System.Drawing.Size(340, 284);
            this.emailContentListBox.TabIndex = 8;
            // 
            // btnEmailSubscribe
            // 
            this.btnEmailSubscribe.Location = new System.Drawing.Point(48, 527);
            this.btnEmailSubscribe.Name = "btnEmailSubscribe";
            this.btnEmailSubscribe.Size = new System.Drawing.Size(248, 36);
            this.btnEmailSubscribe.TabIndex = 9;
            this.btnEmailSubscribe.Text = "Subscribe To Email Service";
            this.btnEmailSubscribe.UseVisualStyleBackColor = true;
            this.btnEmailSubscribe.Click += new System.EventHandler(this.btnEmailSubscribe_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(446, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "SMS Content";
            // 
            // smsContentListBox
            // 
            this.smsContentListBox.FormattingEnabled = true;
            this.smsContentListBox.ItemHeight = 20;
            this.smsContentListBox.Location = new System.Drawing.Point(446, 237);
            this.smsContentListBox.Name = "smsContentListBox";
            this.smsContentListBox.Size = new System.Drawing.Size(326, 284);
            this.smsContentListBox.TabIndex = 8;
            // 
            // btnSMSSubscribe
            // 
            this.btnSMSSubscribe.Location = new System.Drawing.Point(489, 527);
            this.btnSMSSubscribe.Name = "btnSMSSubscribe";
            this.btnSMSSubscribe.Size = new System.Drawing.Size(248, 36);
            this.btnSMSSubscribe.TabIndex = 9;
            this.btnSMSSubscribe.Text = "Subscribe To SMS Service";
            this.btnSMSSubscribe.UseVisualStyleBackColor = true;
            this.btnSMSSubscribe.Click += new System.EventHandler(this.btnSMSSubscribe_Click);
            // 
            // frmInitialRabbitMQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 643);
            this.Controls.Add(this.btnSMSSubscribe);
            this.Controls.Add(this.btnEmailSubscribe);
            this.Controls.Add(this.smsContentListBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.emailContentListBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.smsTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "frmInitialRabbitMQ";
            this.Text = "Initial RabbitMQ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button button1;
        private Button button2;
        private Button button3;
        private Button button4;
        private Label label1;
        private TextBox emailTextBox;
        private Button button5;
        private Label label3;
        private TextBox smsTextBox;
        private Button button7;
        private Label label2;
        private ListBox emailContentListBox;
        private Button btnEmailSubscribe;
        private Label label4;
        private ListBox smsContentListBox;
        private Button btnSMSSubscribe;
    }
}